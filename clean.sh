#!/bin/bash
set -e

for PROJECT in `ls src`
do
  echo "Building $PROJECT"
  (cd src/$PROJECT && make clean)
done
