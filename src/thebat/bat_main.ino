#define DEBUG_OUTPUT
#include <DebugUtils.h>
#include <Ultrasonic.h>

Ultrasonic ranger(12, 13);

void setup(){
  DEBUG_INIT(9600)
}

void loop(){
  int range = ranger.Ranging(CM);
  DEBUG_PRINTLN(range)
}
