#define DEBUG_OUTPUT
#include <DebugUtils.h>
#include <Wire.h>
#include <VL53L0X.h>

VL53L0X lidar;

void setup(){
  DEBUG_INIT(9600)
  DEBUG_PRINTLN(F("Starting Wire..."))
  Wire.begin();
  DEBUG_PRINTLN(F("Initialising lidar..."))
  lidar.init();
  lidar.setTimeout(500);
  lidar.setMeasurementTimingBudget(20000); // high speed
  //lidar.setMeasurementTimingBudget(200000); // high accuracy
  DEBUG_PRINTLN(F("setup complete :-)"))
}

void loop(){
  /*
  Serial.print(lidar.readRangeSingleMillimeters());
  if(lidar.timeoutOccurred()){
    Serial.print(F("TIMEOUT!"));
  }
  Serial.println();
  */
  for(int a = 0; a < 181; a += 10){
    uint16_t l_range = lidar.readRangeSingleMillimeters();
    DEBUG_PRINT(a)
    DEBUG_PRINT(':')
    DEBUG_PRINT(l_range)
    DEBUG_PRINTLN()
    delay(100);
  }
}
