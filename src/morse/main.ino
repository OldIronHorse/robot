#define UNIT 100

const char* msg = "sos";

void setup(){
  pinMode(13, OUTPUT);
}

void flash(int length_ms){
  digitalWrite(13, HIGH);
  delay(length_ms);
  digitalWrite(13, LOW);
}

void dit(){
  flash(UNIT);
  delay(UNIT);
}

void dah(){
  flash(UNIT * 3);
  delay(UNIT);
}

void char_to_morse(char c){
  switch(c) {
    case 's':
      dit();
      dit();
      dit();
      break;
    case 'o':
      dah();
      dah();
      dah();
      break;
  }
}

void loop(){
  for(int i = 0; msg[i] != '\0'; i++){
    char_to_morse(msg[i]);
    delay(UNIT * 2);
  }
  delay(UNIT * 4);
}
