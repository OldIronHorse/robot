#ifndef PedestrianLight_h
#define PedestrianLight_h

class PedestrianLight{
  public:
    enum State {STOP, GO};

    PedestrianLight(int pin_red,int pin_green);
    void init();
    void state(State state);
    State state();

  private:
    void render();

    State _state;
    int _pin_red;
    int _pin_green;
};

#endif
