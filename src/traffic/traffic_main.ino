#include "Intersection3.h"

PedestrianLight all_peds(3,4);
VehicleLight ns_vehicle(5,6,7);
VehicleLight ew_vehicle(8,9,10);
Intersection3 intersection(ns_vehicle,ew_vehicle,all_peds);

void setup(){
  intersection.init();
}

void loop(){
    intersection.next_dwell();
}
