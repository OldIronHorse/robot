#include <Arduino.h>
#include "Intersection3.h"

const Intersection3::State Intersection3::_states[INTERSECTION_STATE_COUNT] = {
    {VehicleLight::STOP, VehicleLight::STOP, PedestrianLight::STOP, 2000},
    {VehicleLight::WAIT, VehicleLight::STOP, PedestrianLight::STOP, 2000},
    {VehicleLight::GO, VehicleLight::STOP, PedestrianLight::STOP, 10000},
    {VehicleLight::CAUTION, VehicleLight::STOP, PedestrianLight::STOP, 2000},
    {VehicleLight::STOP, VehicleLight::STOP, PedestrianLight::STOP, 2000},
    {VehicleLight::STOP, VehicleLight::WAIT, PedestrianLight::STOP, 2000},
    {VehicleLight::STOP, VehicleLight::GO, PedestrianLight::STOP, 10000},
    {VehicleLight::STOP, VehicleLight::CAUTION, PedestrianLight::STOP, 2000},
    {VehicleLight::STOP, VehicleLight::STOP, PedestrianLight::STOP, 2000},
    {VehicleLight::STOP, VehicleLight::STOP, PedestrianLight::GO, 15000}
};

Intersection3::Intersection3(VehicleLight& ns_vehicle,
                             VehicleLight& ew_vehicle,
                             PedestrianLight& peds)
:_ns_vehicle(ns_vehicle),_ew_vehicle(ew_vehicle),_peds(peds),_state(0),_lastTransition(0){;}

void Intersection3::init(){
  _ns_vehicle.init();
  _ew_vehicle.init();
  _peds.init();
  _state = 0;
  _lastTransition = millis();
  render();
}

void Intersection3::next(){
    _state++;
    if(_state >= INTERSECTION_STATE_COUNT){
        _state = 0;
    }
    _lastTransition = millis();
    render();
}

void Intersection3::next_dwell(){
    if(millis() - _lastTransition > _states[_state].dwell_ms){
        next();
    }
}

void Intersection3::render(){
    _ns_vehicle.state(_states[_state].ns);
    _ew_vehicle.state(_states[_state].ew);
    _peds.state(_states[_state].ped);
}
