#ifndef Intersection3_h
#define Intersection3_h

#include "VehicleLight.h"
#include "PedestrianLight.h"

#define INTERSECTION_STATE_COUNT 10

class Intersection3{
  public:
    Intersection3(VehicleLight& ns_vehicle,
                  VehicleLight& ew_vehicle,
                  PedestrianLight& peds);

    void init();
    void next();
    void next_dwell();

  private:
    void render();

    enum Lights {NS_VEHICLE, EW_VEHICLE, PEDESTRIAN};
    struct State {
        VehicleLight::State ns;
        VehicleLight::State ew;
        PedestrianLight::State ped;
        unsigned int dwell_ms;
    };
    static const State _states[INTERSECTION_STATE_COUNT];
  public: //test
    int _state;
    unsigned int _lastTransition;
    VehicleLight& _ns_vehicle;
    VehicleLight& _ew_vehicle;
    PedestrianLight& _peds;
};

#endif
