#ifndef VehicleLight_h
#define VehicleLight_h

class VehicleLight{
  public:
    VehicleLight(int pin_red,int pin_amber,int pin_green);
    void init();

    enum State {STOP, WAIT, GO, CAUTION};
    State state();
    void state(State state);

  private:
    void render();

    int _pin_red;
    int _pin_amber;
    int _pin_green;

    State _state;
};

#endif
