#include <testing.h>
#include <Arduino.h>
#include "../Intersection3.h"

VehicleLight v_light(3,4,5);

void vehicle_set_up(){
  MockArduino::instance().reset();
  v_light.init();
}

DEFINE_TEST(vehicle_initial_state)
  assertEqual(VehicleLight::STOP,v_light.state());
  assertEqual(OUTPUT,MockArduino::instance().pin_mode[3]);
  assertEqual(OUTPUT,MockArduino::instance().pin_mode[4]);
  assertEqual(OUTPUT,MockArduino::instance().pin_mode[5]);
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  assertEqual(LOW,MockArduino::instance().pin_out[5]);
}

DEFINE_TEST(vehicle_stop)
  v_light.state(VehicleLight::STOP);
  assertEqual(VehicleLight::STOP,v_light.state());
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  assertEqual(LOW,MockArduino::instance().pin_out[5]);
}

DEFINE_TEST(vehicle_wait)
  v_light.state(VehicleLight::WAIT);
  assertEqual(VehicleLight::WAIT,v_light.state());
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(HIGH,MockArduino::instance().pin_out[4]);
  assertEqual(LOW,MockArduino::instance().pin_out[5]);
}

DEFINE_TEST(vehicle_go)
  v_light.state(VehicleLight::GO);
  assertEqual(VehicleLight::GO,v_light.state());
  assertEqual(LOW,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  assertEqual(HIGH,MockArduino::instance().pin_out[5]);
}

DEFINE_TEST(vehicle_caution)
  v_light.state(VehicleLight::CAUTION);
  assertEqual(VehicleLight::CAUTION,v_light.state());
  assertEqual(LOW,MockArduino::instance().pin_out[3]);
  assertEqual(HIGH,MockArduino::instance().pin_out[4]);
  assertEqual(LOW,MockArduino::instance().pin_out[5]);
}

BEGIN_TEST_SUITE(vehicle_light_tests)
ADD_TEST(vehicle_initial_state)
ADD_TEST(vehicle_stop)
ADD_TEST(vehicle_wait)
ADD_TEST(vehicle_go)
ADD_TEST(vehicle_caution)
END_TEST_SUITE

PedestrianLight p_light(3,4);

void pedestrian_set_up(){
  MockArduino::instance().reset();
  p_light.init();
}

DEFINE_TEST(pedestrian_initial_state)
  assertEqual(OUTPUT,MockArduino::instance().pin_mode[3]);
  assertEqual(OUTPUT,MockArduino::instance().pin_mode[4]);
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
}

DEFINE_TEST(pedestrian_stop)
  p_light.state(PedestrianLight::STOP);
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
}

DEFINE_TEST(pedestrian_go)
  p_light.state(PedestrianLight::GO);
  assertEqual(LOW,MockArduino::instance().pin_out[3]);
  assertEqual(HIGH,MockArduino::instance().pin_out[4]);
}

BEGIN_TEST_SUITE(pedestrian_light_tests)
ADD_TEST(pedestrian_initial_state)
ADD_TEST(pedestrian_stop)
ADD_TEST(pedestrian_go)
END_TEST_SUITE

// Test suite for simple 2-way plus pedestrian intersection (3-phase)

PedestrianLight all_peds(3,4);
VehicleLight ns_vehicle(5,6,7);
VehicleLight ew_vehicle(8,9,10);

Intersection3 inter3(ns_vehicle,ew_vehicle,all_peds);

void intersection_3_set_up(void){
  MockArduino::instance().reset();
  inter3.init();
}

DEFINE_TEST(intersection_3_sequence_0)
  // pedestrian stop
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  // North-South stop
  assertEqual(HIGH,MockArduino::instance().pin_out[5]);
  assertEqual(LOW,MockArduino::instance().pin_out[6]);
  assertEqual(LOW,MockArduino::instance().pin_out[7]);
  // East-West stop
  assertEqual(HIGH,MockArduino::instance().pin_out[8]);
  assertEqual(LOW,MockArduino::instance().pin_out[9]);
  assertEqual(LOW,MockArduino::instance().pin_out[10]);
}

DEFINE_TEST(intersection_3_sequence_1)
  inter3.next();
  // pedestrian stop
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  // North-South wait
  assertEqual(HIGH,MockArduino::instance().pin_out[5]);
  assertEqual(HIGH,MockArduino::instance().pin_out[6]);
  assertEqual(LOW,MockArduino::instance().pin_out[7]);
  // East-West stop
  assertEqual(HIGH,MockArduino::instance().pin_out[8]);
  assertEqual(LOW,MockArduino::instance().pin_out[9]);
  assertEqual(LOW,MockArduino::instance().pin_out[10]);
}

DEFINE_TEST(intersection_3_sequence_2)
  inter3.next();
  inter3.next();
  // pedestrian stop
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  // North-South go
  assertEqual(LOW,MockArduino::instance().pin_out[5]);
  assertEqual(LOW,MockArduino::instance().pin_out[6]);
  assertEqual(HIGH,MockArduino::instance().pin_out[7]);
  // East-West stop
  assertEqual(HIGH,MockArduino::instance().pin_out[8]);
  assertEqual(LOW,MockArduino::instance().pin_out[9]);
  assertEqual(LOW,MockArduino::instance().pin_out[10]);
}

DEFINE_TEST(intersection_3_sequence_3)
  inter3.next();
  inter3.next();
  inter3.next();
  // pedestrian stop
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  // North-South caution
  assertEqual(LOW,MockArduino::instance().pin_out[5]);
  assertEqual(HIGH,MockArduino::instance().pin_out[6]);
  assertEqual(LOW,MockArduino::instance().pin_out[7]);
  // East-West stop
  assertEqual(HIGH,MockArduino::instance().pin_out[8]);
  assertEqual(LOW,MockArduino::instance().pin_out[9]);
  assertEqual(LOW,MockArduino::instance().pin_out[10]);
}

DEFINE_TEST(intersection_3_sequence_4)
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  // pedestrian stop
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  // North-South stop
  assertEqual(HIGH,MockArduino::instance().pin_out[5]);
  assertEqual(LOW,MockArduino::instance().pin_out[6]);
  assertEqual(LOW,MockArduino::instance().pin_out[7]);
  // East-West stop
  assertEqual(HIGH,MockArduino::instance().pin_out[8]);
  assertEqual(LOW,MockArduino::instance().pin_out[9]);
  assertEqual(LOW,MockArduino::instance().pin_out[10]);
}    

DEFINE_TEST(intersection_3_sequence_5)
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  // pedestrian stop
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  // North-South stop
  assertEqual(HIGH,MockArduino::instance().pin_out[5]);
  assertEqual(LOW,MockArduino::instance().pin_out[6]);
  assertEqual(LOW,MockArduino::instance().pin_out[7]);
  // East-West wait 
  assertEqual(HIGH,MockArduino::instance().pin_out[8]);
  assertEqual(HIGH,MockArduino::instance().pin_out[9]);
  assertEqual(LOW,MockArduino::instance().pin_out[10]);
}    

DEFINE_TEST(intersection_3_sequence_6)
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  // pedestrian stop
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  // North-South stop
  assertEqual(HIGH,MockArduino::instance().pin_out[5]);
  assertEqual(LOW,MockArduino::instance().pin_out[6]);
  assertEqual(LOW,MockArduino::instance().pin_out[7]);
  // East-West go
  assertEqual(LOW,MockArduino::instance().pin_out[8]);
  assertEqual(LOW,MockArduino::instance().pin_out[9]);
  assertEqual(HIGH,MockArduino::instance().pin_out[10]);
}    

DEFINE_TEST(intersection_3_sequence_7)
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  // pedestrian stop
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  // North-South stop
  assertEqual(HIGH,MockArduino::instance().pin_out[5]);
  assertEqual(LOW,MockArduino::instance().pin_out[6]);
  assertEqual(LOW,MockArduino::instance().pin_out[7]);
  // East-West caution
  assertEqual(LOW,MockArduino::instance().pin_out[8]);
  assertEqual(HIGH,MockArduino::instance().pin_out[9]);
  assertEqual(LOW,MockArduino::instance().pin_out[10]);
}    

DEFINE_TEST(intersection_3_sequence_8)
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  // pedestrian stop
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  // North-South stop
  assertEqual(HIGH,MockArduino::instance().pin_out[5]);
  assertEqual(LOW,MockArduino::instance().pin_out[6]);
  assertEqual(LOW,MockArduino::instance().pin_out[7]);
  // East-West stop
  assertEqual(HIGH,MockArduino::instance().pin_out[8]);
  assertEqual(LOW,MockArduino::instance().pin_out[9]);
  assertEqual(LOW,MockArduino::instance().pin_out[10]);
}    

DEFINE_TEST(intersection_3_sequence_9)
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  // pedestrian go
  assertEqual(LOW,MockArduino::instance().pin_out[3]);
  assertEqual(HIGH,MockArduino::instance().pin_out[4]);
  // North-South stop
  assertEqual(HIGH,MockArduino::instance().pin_out[5]);
  assertEqual(LOW,MockArduino::instance().pin_out[6]);
  assertEqual(LOW,MockArduino::instance().pin_out[7]);
  // East-West stop
  assertEqual(HIGH,MockArduino::instance().pin_out[8]);
  assertEqual(LOW,MockArduino::instance().pin_out[9]);
  assertEqual(LOW,MockArduino::instance().pin_out[10]);
}    

DEFINE_TEST(intersection_3_sequence_10)
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  // pedestrian stop
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  // North-South stop
  assertEqual(HIGH,MockArduino::instance().pin_out[5]);
  assertEqual(LOW,MockArduino::instance().pin_out[6]);
  assertEqual(LOW,MockArduino::instance().pin_out[7]);
  // East-West stop
  assertEqual(HIGH,MockArduino::instance().pin_out[8]);
  assertEqual(LOW,MockArduino::instance().pin_out[9]);
  assertEqual(LOW,MockArduino::instance().pin_out[10]);
}    

DEFINE_TEST(intersection_3_sequence_11)
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  inter3.next();
  // pedestrian stop
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  // North-South wait
  assertEqual(HIGH,MockArduino::instance().pin_out[5]);
  assertEqual(HIGH,MockArduino::instance().pin_out[6]);
  assertEqual(LOW,MockArduino::instance().pin_out[7]);
  // East-West stop
  assertEqual(HIGH,MockArduino::instance().pin_out[8]);
  assertEqual(LOW,MockArduino::instance().pin_out[9]);
  assertEqual(LOW,MockArduino::instance().pin_out[10]);
}    
// TODO: what about wait and button and timers?

BEGIN_TEST_SUITE(intersection_3_tests)
ADD_TEST(intersection_3_sequence_0)
ADD_TEST(intersection_3_sequence_1)
ADD_TEST(intersection_3_sequence_2)
ADD_TEST(intersection_3_sequence_3)
ADD_TEST(intersection_3_sequence_4)
ADD_TEST(intersection_3_sequence_5)
ADD_TEST(intersection_3_sequence_6)
ADD_TEST(intersection_3_sequence_7)
ADD_TEST(intersection_3_sequence_8)
ADD_TEST(intersection_3_sequence_9)
ADD_TEST(intersection_3_sequence_10)
ADD_TEST(intersection_3_sequence_11)
END_TEST_SUITE

void dwell_set_up(void){
  MockArduino::instance().reset();
  inter3.init();
}

DEFINE_TEST(dwell_0_not_elapsed)
  MockArduino::instance().millis = 2990;
  inter3.next_dwell();
  // pedestrian stop
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  // North-South stop
  assertEqual(HIGH,MockArduino::instance().pin_out[5]);
  assertEqual(LOW,MockArduino::instance().pin_out[6]);
  assertEqual(LOW,MockArduino::instance().pin_out[7]);
  // East-West stop
  assertEqual(HIGH,MockArduino::instance().pin_out[8]);
  assertEqual(LOW,MockArduino::instance().pin_out[9]);
  assertEqual(LOW,MockArduino::instance().pin_out[10]);
}
    
DEFINE_TEST(dwell_0_elapsed)
  MockArduino::instance().millis = 3010;
  cout << inter3._lastTransition << endl;
  inter3.next_dwell();
  // pedestrian stop
  assertEqual(HIGH,MockArduino::instance().pin_out[3]);
  assertEqual(LOW,MockArduino::instance().pin_out[4]);
  // North-South stop
  assertEqual(HIGH,MockArduino::instance().pin_out[5]);
  assertEqual(HIGH,MockArduino::instance().pin_out[6]);
  assertEqual(LOW,MockArduino::instance().pin_out[7]);
  // East-West stop
  assertEqual(HIGH,MockArduino::instance().pin_out[8]);
  assertEqual(LOW,MockArduino::instance().pin_out[9]);
  assertEqual(LOW,MockArduino::instance().pin_out[10]);
}

BEGIN_TEST_SUITE(dwell_tests)
ADD_TEST(dwell_0_not_elapsed)
ADD_TEST(dwell_0_elapsed)
END_TEST_SUITE

int main(void){
  return run(vehicle_light_tests,vehicle_set_up) +
         run(pedestrian_light_tests,pedestrian_set_up) +
         run(intersection_3_tests,intersection_3_set_up) +
         run(dwell_tests,dwell_set_up);
}
