#!/bin/bash
set -e

PROJECTS="rover image traffic serial_remote ir_rover ultramin rover serial_rover mearm avoid"
for PROJECT in $PROJECTS
do
  (cd src/$PROJECT && make run_tests)
done
